# AB-tests

В данном проекте находится Python код (Jupyter Notebook), при помощи которого проверяется корректность системы сплитования перед проведением АБ-теста:

а) Отбор серии сэмплов из двух групп, проверка валидности полученных сэмплов 

б) Проверка распределения p-value при 10000 итерациях сэмплирования 

в) Проверка % p-value, при которых мы могли бы ошибочно отклонить H0

<br>

*Права на задания и учебные материалы принадлежат Karpov Courses (https://karpov.courses/)*



